<!doctype html>
<html  lang="fr-FR">
   <head>
      <meta charset="utf-8">
      <link rel="stylesheet" href="/static/styles.css" type="text/css">
   </head>
   <body>
   <h1><a class = titre href = "/">SAYFLIX</a></h1>
% titres = ["Pseudo", "E-mail", "Date de naissance", "Mot de passe"]

<table border = 1>
  <thead>
      %for titre in titres:
     <td>{{titre}}</td>
     %end
  </thead>
    % for ligne in lignes:
        <tr>
        %for col in ligne:
        <td>{{col}}</td>
        %end
        </tr>
    % end
</table>

</body>
</html>
