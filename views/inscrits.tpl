<!doctype html>
<html  lang="fr-FR">
   <head><meta charset="utf-8">
   <link rel="stylesheet" href="/static/styles.css" type="text/css">
   </head>
   <body>
      <h1><a class = titre href = "/">SAYFLIX</a></h1>
      <form action = "/ajouteEntree" method = "POST">
         <h3>Inscription</h3>
         Pseudo<br>
         <input type = "text" name = "pseudo" id = "pseudo" required/><br>
         
         Date de naissance<br>
         <input type = "text" name = "nais" id = "nais" placeholder = "2002-02-29" pattern="[1-2][0-9]{3}-[0-1][0-9]-[0-3][0-9]" required/><br>

         E-mail<br>
         <input type = "email" name = "email" id="email" required/><br>

         Mot de passe<br>
         <input type = "password" name = "mdp" id = "mdp" required/><br>

         <input type = "submit" value = "Envoyer" id = "envoyer"/><br>
      </form>
   </body>
</html>