<!doctype html>
<html  lang="fr-FR">
   <head><meta charset="utf-8">
   <link rel="stylesheet" href="/static/styles.css" type="text/css">
   </head>
   <body>
   <h1><a class = titre href = "/">SAYFLIX</a></h1>
<h3>Résultat de la requête {{rekete}} sur la colonne {{champ}} </h3>

% titres = ["Titre", "Date", "Réalisateur"]

<table border = 1>
  <thead>
      %for titre in titres:
     <td>{{titre}}</td>
     %end
  </thead>
  
%l = len(res)

  <p>{{ l }} résultats trouvés</p>

%for ligne in res:
    <tr>
    % for col in ligne:
        <td>{{col}}</td>
    % end
     </tr>
%end
</table>

<a href = "/recherchefilms">Rechercher un film</a>

</body>
</html>
