<!DOCTYPE html>
<html lang="fr">
  <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/static/styles.css" type="text/css">
  </head>
  <body>
        <h1><a class = titre href = "/">SAYFLIX</a></h1>
        <h3>Azur et Asmar</h3>
        <img class = "affiche" src="https://fr.web.img4.acsta.net/medias/nmedia/18/35/52/37/18654088.jpg" alt="" style="width:300px;height:400px;"> 
        <br />
        <br />
        <form>
            Azur et Asmar, deux enfants bercés par les mêmes légendes, s'aiment comme deux frères. 
            Jenane, leur nourrice, éduque son fils Asmar, brun aux yeux noirs, ainsi que celui de son maître, Azur, blond aux yeux bleus.
            À l'âge de raison, les garçons sont brutalement séparés. Le père d'Azur envoie son fils étudier en ville et chasse la nourrice et Asmar.
        </form>
        <br />
        <br />
        <div class = "video-responsive"><iframe width="560" height="315" src="https://www.youtube.com/embed/6HQ9PccqswY" title="YouTube video player" 
        frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  </body>
</html>