from bottle import route, post, install, template, request, run, static_file
from bottle_sqlite import SQLitePlugin

install(SQLitePlugin(dbfile='./basestreaming.db'))

#fichier pour chaque film
@route('/<filename>')
def fiche(filename):
    return template(filename)

#feuille de style
@route('/static/<filename>')
def server_static(filename):
    return static_file(filename, root='static')

# La racine du site
@route('/')
def index():
   return template('indexni.tpl')
 
 # le formulaire d'inscription
@route('/entreNouveau')
def entreNouveau():
   return template('inscrits.tpl')

# le formulaire d'ajout d'un film
@route('/addfilm')
def entreNouveaufilms():
   return template('addfilm.tpl')


# inscription des données d'un inscrit du formulaire dans la BDD
@post('/ajouteEntree')
def ajouteEntree(db):
    pseudo = request.forms['pseudo']
    email = request.forms['email']
    nais = request.forms['nais']
    mdp = request.forms['mdp']
    db.execute("""
        INSERT INTO Inscrits (pseudo, email, nais, mdp )
         VALUES (?,?,?,?)
        """,(pseudo, email, nais, mdp)
        )
    return template("indexni.tpl")

@route('/liste')
def liste(db):
    req = db.execute("select * from Inscrits")
    lignes = req.fetchall()
    return template("listeni.tpl", lignes=lignes)

# inscription des données d'un film du formulaire dans la BDD
@post('/ajoutefilm')
def addfilm(db):
    titre = request.forms['titre']
    datedesortie = request.forms['datedesortie']
    real = request.forms['real']
    db.execute("""
        INSERT INTO films (titre, datedesortie, real)
         VALUES (?,?,?)
        """,(titre, datedesortie, real)
        )
    return template("indexni.tpl")

@route('/listefilms')
def listefilm(db):
    req = db.execute("select * from films")
    lignes = req.fetchall()
    return template("listefilm.tpl", lignes=lignes)

# moteur de recherche
@route('/recherche')
def recherche():
   return template('rechercheinscrits.tpl')

@route('/recherchefilms')
def recherchefilms():
    return template('recherchefilms.tpl')

# resultats du moteur de recherche
@post('/requete')
def requete(db):
    champ = request.forms.champ
    rekete = f"%{request.forms['motcle']}%"
    lignes = db.execute(f"SELECT * FROM Inscrits WHERE {champ} LIKE ?", (rekete,))
    #lignes = db.executescript("SELECT * FROM Inscrits WHERE nom LIKE '" + str(rekete) +"'") #si vous voulez jouer avec Little Bob
    res = lignes.fetchall()
    return template("requeteinscrits.tpl",res=res, rekete=rekete, champ=champ )#msg=msg)

@post('/requetefilms')
def requetefilms(db):
    champ = request.forms.champ
    rekete = f"%{request.forms['motcle']}%"
    lignes = db.execute(f"SELECT * FROM films WHERE {champ} LIKE ?", (rekete,))
    res = lignes.fetchall()
    return template("requetefilms.tpl",res=res, rekete=rekete, champ=champ )

# on ouvre un serveur en local sur le port 7000 par exemple
if __name__ == '__main__':
   run(reloader = True, debug = True, host='127.0.0.7', port=7000)



